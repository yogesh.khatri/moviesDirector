// importing connection file 
const connection = require('./connection')
const moviesData = require("./movies.json")

// preparing sql query
let sql = "INSERT INTO directors (director) VALUES ?";
let count = 1;
let directorObj = {};

// getting data in object for unique id
for (let i = 0; i < moviesData.length; i++) {
    if (moviesData[i]["Director"] in directorObj) {
        continue;
    }
    else {
        directorObj[moviesData[i]["Director"]] = count;
        count += 1;
    }
}
console.log("entering data in director")

// getting data in director list for insertion in director Table
let directorList = [];
for (let director in directorObj) {
    let temp = [];
    temp.push(director);
    directorList.push(temp);
}
// // inserting data in director table
// connection.db.query(sql, [directorList], function (err, result) {
//     if (err) throw err;
//     console.log("Number of records inserted: " + result.affectedRows);
// })


// ------------------------------------------------------------------------

// getting data to enter in movies Table
let moviesList = [];
for (let i = 0; i < moviesData.length; i++) {
    let temp = [];
    for (let field in moviesData[i]) {
        if (field == "Director") {
            // do nothing
        } else {
            temp.push(moviesData[i][field]);
        }
    }
    temp.push(directorObj[moviesData[i]["Director"]]);
    moviesList.push(temp);
}
sql = "insert into movies (Rank_id, Title, Description,  Runtime,genre, rating, metascore, votes,Gross_Earning_in_Mil,Actor ,Year,Director_id) values?"
console.log(moviesList);
// inserting data in movies table
connection.db.query(sql, [moviesList], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
});
