var express = require('express');
var router = express.Router();
var MoviesQuery = require('./MoviesQuery');
var DirectorQuery = require('./DirectorQuery')
var bodyParser = require('body-parser');

router.use(bodyParser.json());

// calling function to get all directors
router.get('/api/directors', function (req, res) {
    DirectorQuery.getAll()
        .then((result) => {
            res.send(result);
        }).catch((err) => {
            res.status(404).send({ data: { error: `Data Not Found` } })
        })
});

// calling function get details from id
router.get('/api/directors/:id', function (req, res) {
    DirectorQuery.getWithGivenId(req.params.id)
        .then((result) => {
            if (result.length > 0) {
                res.send(result);
            } else {
                res.status(404).json({ msg: `ID not found ${req.params.id}` });
            }
        })
});

// calling delete function
router.delete('/api/directors/:id', function (req, res) {
    DirectorQuery.deleteWithGivenId(req.params.id)
        .then((result) => {
            if (result["affectedRows"] != 0) {
                res.send("Item deleted successfully " + req.params.id)
            }
            else {
                res.status(404).json({ msg: `No member with the id of ${req.params.id}` });
            }
        })
        .catch((error) => {
            console.log("Invalid input" + error)
        })
});


// adding new director
router.post('/api/directors', (req, res) => {
    DirectorQuery.addNewDirector(req.body.director)
        .then((result) => {
            if (result["affectedRows"] != 0) {
                res.send("item added successfully")
            }
            else {
                res.status(422).json({ msg: `No member with the id o` });
            }
        })
})

// updating  director
router.put('/api/directors/:id', (req, res) => {
    DirectorQuery.updateDirectorWithGivenId(req.params.id, req.body.director)
        .then((result) => {
            if (result["affectedRows"] != 0) {
                res.send("item updated successfully")
            }
            else {
                res.status(404).json({ msg: `No member with the id of ${req.params.id}` });
            }
        })
})

// ------------------------------------------------------------------------------------------------------
// calling function to get all movies
router.get('/api/movies', function (req, res) {
    MoviesQuery.getAll()
        .then((result) => {
            if (result.length > 0) {
                res.send(result);
            } else {
                res.status(404).json({ msg: `No member with the id of ${req.params.id}` });
            }
        }).catch((err) => {
            res.status(404).send({ data: { error: `can't retrieve data` } })
        })
});

// calling function to get movies by  id
router.get('/api/movies/:id', function (req, res) {
    MoviesQuery.getWithGivenId(req.params.id)
        .then((result) => {
            if (result.length > 0) {
                res.send(result);
            } else {
                res.status(404).json({ msg: `No member with the id of ${req.params.id}` });
            }
        })
});

// calling delete movies
router.delete('/api/movies/:id', function (req, res) {
    MoviesQuery.deleteWithGivenId(req.params.id)
        .then((result) => {
            if (result["affectedRows"] != 0) {
                res.send("Item deleted successfully " + req.params.id)
            }
            else {
                res.status(404).json({ msg: `No member with the id of ${req.params.id}` });
            }
        })
        .catch((error) => {
            console.log("Invalid input" + error)
        })
});

// adding new movie
router.post('/api/movies', (req, res) => {
    MoviesQuery.addNewMovie(req.body.rank, req.body.title, req.body.description, req.body.runtime, req.body.genre, req.body.rating, req.body.metascore, req.body.votes, req.body.Gross_Earning_in_Mil, req.body.actor, req.body.year, req.body.Director_id)
        .then((result) => {
            if (result["affectedRows"] != 0) {
                res.send("item added successfully")
            }
            else {
                res.status(422).json({ msg: `No member with the id of ${req.params.id}` });
            }
        })
})

// updating movie form  json file
router.put('/api/movies/:id', (req, res) => {
    console.log(req.body, req.params.id)
    MoviesQuery.updateMovieWithGivenId(req.body, req.params.id)
        .then((result) => {
            if (result["affectedRows"] != 0) {
                res.send("item updated successfully")
            }
            else {
                res.status(404).json({ msg: `No member with the id of ${req.params.id}` });
            }
        })
})




module.exports = router;