const connection = require('./connection');

// get Movie with movie id
function getWithGivenId(id) {
    return new Promise((resolve, reject) => {
        connection.db.query(`select * from movies  where id=${id};`, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

// get all Movies
function getAll() {
    return new Promise((resolve, reject) => {
        connection.db.query(`select * from movies;`, function (err, result) {
            if (err) {
                reject(err);
            }
            resolve(result);
        })
    });
}

// delete row from id
function deleteWithGivenId(id) {
    return new Promise((resolve, reject) => {
        let sql = `delete from movies where id=${id}`
        connection.db.query(sql, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

//add new field in movies
function addNewMovie(rank = null, Title = null, description = null, runtime = null, genre = null, rating = null, metascore = null, votes = null, Gross_Earning_in_Mil = null, actor = null, year = null, Director_id = null) {
    return new Promise((resolve, reject) => {
        let sql = `insert into movies (rank,Title,description, runtime, genre, rating, metascore,votes,Gross_Earning_in_Mil,actor,year,Director_id) values (${rank},"${Title}","${description}",${runtime},"${genre}",${rating},"${metascore}",${votes},"${Gross_Earning_in_Mil}","${actor}",${year},${Director_id})`;
        connection.db.query(sql, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

// updating movies tables 
function updateMovieWithGivenId(objOfValues, id) {
    return new Promise((resolve, reject) => {
        let sql = `update movies  set `;
        for (let key in objOfValues) {
            if (key == 'rank') {
                sql += ` ${key}=${objOfValues[key]} `;
                continue;
            }
            sql += `, ${key}="${objOfValues[key]}" `
        }
        sql += ` where id=${id};`;

        connection.db.query(sql, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

module.exports = {
    updateMovieWithGivenId,
    addNewMovie,
    getWithGivenId,
    deleteWithGivenId,
    getAll
}