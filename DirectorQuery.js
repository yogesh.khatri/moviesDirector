const con = require('./connection');

// get Director with given id
function getWithGivenId(id) {
    return new Promise((resolve, reject) => {
        con.db.query(`select * from directors  where id=${id};`, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

// get all Directors
function getAll() {
    return new Promise((resolve, reject) => {
        con.db.query(`select * from directors;`, function (err, result) {
            if (err) {
                reject(err);
            }
            resolve(result);
        })
    });
}
// delete row of given id 
function deleteWithGivenId(id) {
    return new Promise((resolve, reject) => {
        let sql = `delete from directors  where id=${id}`
        con.db.query(sql, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

// add new director with director name
function addNewDirector(directorName) {
    return new Promise((resolve, reject) => {
        let sql = `insert into directors (Director) values("${directorName}")`;
        console.log("sql for adding director " + sql)
        con.db.query(sql, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

// updating directors tables 
function updateDirectorWithGivenId(id, directorName) {
    return new Promise((resolve, reject) => {
        let sql = `update directors set director="${directorName}" where id=${id}`;
        con.db.query(sql, function (err, result, fields) {
            if (err) {
                reject(err);
            }
            resolve(result)
        })
    })
}

module.exports = {
    updateDirectorWithGivenId,
    addNewDirector,
    getWithGivenId,
    deleteWithGivenId,
    getAll
}