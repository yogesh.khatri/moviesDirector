// importing express and database connection file
const express = require('express');
const db = require('./connection');
var route = require('./routs');

// getting functions of express in app
const app = express();
const PORT=process.env.PORT || 3000;
app.use('', route);

// starting server at 3000 port
app.listen(PORT, () => {
    console.log("Server started at port 3000");
})